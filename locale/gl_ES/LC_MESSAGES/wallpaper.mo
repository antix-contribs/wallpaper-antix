��          t      �         	             )     8     ?  2   P  
   �     �  Z   �     �  �       �     �          #     *  1   H     z     �  g   �     �            
         	                                       All Files Default Color Default Folder Images No file selected Note: you cannot change the color with rox desktop Open Image Options This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper - help Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-28 21:09+0200
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
X-Poedit-SourceCharset: UTF-8
 Todos os ficheiros Cor predeterminada Cartafol predeterminado Imaxes Ningún ficheiro seleccionado Nota: co escritorio rox non se pode cambiar a cor Abrir imaxe Opcións Este é un aplicativo de antiX para configurar a imaxe de fondo nos xestores de lapelas predeterminadas Imaxe de fondo do antiX - axuda 
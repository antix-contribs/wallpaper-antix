��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    5   �     �                     #     *     >  	   C     M     S     [     o     x  &   �     �     �     �     �     �               5  	   C     M  g   c     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-05-03 17:48+0300
Last-Translator: José Vieira <jvieira33@sapo.pt>
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Tempo entre alteração de imagens de fundo (Minutos) Sobre Todos os ficheiros Aplicar Centrar Fechar Pasta pré-definida Erro Preencher Ajuda Imagens Sem imagem de fundo Opções Imagem de fundo aleatória Imagem de fundo aleatória temporizada Dimensionar Escolher Cor Escolher a Pasta Escolher a Pasta... Escolher a Imagem... Escolher Imagem Escolher a cor do fundo Ativar imagem Estática Atualizado com êxito Esta é uma aplicação do antiX para definir a imagem de fundo nos gestores de janelas pré-instalados Imagem de fundo do antiX 
��          t      �         	             )     8     ?  2   P  
   �     �  Z   �     �  �    
   �     �     �     
       3   !     U     a  Y   n     �            
         	                                       All Files Default Color Default Folder Images No file selected Note: you cannot change the color with rox desktop Open Image Options This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper - help Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-16 16:24+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
X-Poedit-SourceCharset: UTF-8
 Alle filer Standard farge Standard filmappe Bilder Ingen fil valgt Merk: du kan ikke bytte fargen til rox-skrivebordet Åpne bilde Alternativer Dette er et antiX-program for å velge bakgrunn på de forhåndsinstallerte skrivebordene antiX Wallpaper - hjelp 
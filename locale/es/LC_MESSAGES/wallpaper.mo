��          t      �         	             )     8     ?  2   P  
   �     �  Z   �     �  �                  %  	   9  !   C  8   e     �     �  p   �     %            
         	                                       All Files Default Color Default Folder Images No file selected Note: you cannot change the color with rox desktop Open Image Options This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper - help Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-29 21:07+0200
PO-Revision-Date: 2019-10-13 17:48+0300
Last-Translator: German Lancheros <glancheros2015@gmail.com>
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Todos los archivos Color por defecto Carpeta por defecto Imágenes No se seleccionó ningún archivo Nota: no se puede cambiar el color con el escritorio rox Abrir imagen Opciones Esta aplicación de antiX le permite seleccionar su papel tapiz en los administradores de ventanas preinstalados Papel Tapiz de antiX - ayuda 